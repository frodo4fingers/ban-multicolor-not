
--[[
                                     
     Multicolor Awesome WM theme 2.0
     github.com/copycat-killer       
                                     
--]]

local beautiful     = require("beautiful")
local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local os    = { getenv = os.getenv, setlocale = os.setlocale }
local spotify_widget = require("awesome-wm-widgets.spotify-widget.spotify")

local theme                                     = {}
theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/multicolor"
-- theme.wallpaper                                 = theme.confdir .. "/wall.png"
theme.wallpaper                                 = "/home/frodo/Bilder/Painting-City-Full-HD-Wallpapers.jpg"
-- theme.font                                      = "xos4 Terminus 8"
theme.font                                      = "Liberation Sans 10.5"
theme.menu_bg_normal                            = "#333333"
theme.menu_bg_focus                             = "#333333"
theme.bg_normal                                 = "#333333"
theme.bg_focus                                  = "#ffffff" -- "#0991FF"
theme.bg_urgent                                 = "#333333"
theme.fg_normal                                 = "#bbbbbb"
theme.fg_focus                                  = "#333333"
theme.fg_urgent                                 = "#FF530D"
theme.fg_minimize                               = "#ffffff"
theme.border_width                              = 2
theme.border_normal                             = "#1c2022"
theme.border_focus                              = "#ffffff" -- "#0991FF"
theme.border_marked                             = "#ffffff" -- "#0991FF"
theme.menu_border_width                         = 0
theme.menu_width                                = 160
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                            = "#aaaaaa"
theme.menu_fg_focus                             = "#ffffff" -- "#0991FF"
theme.menu_bg_normal                            = "#333333"
theme.menu_bg_focus                             = "#333333"
theme.widget_temp                               = theme.confdir .. "/icons/temp_dark.png"
theme.widget_uptime                             = theme.confdir .. "/icons/ac.png"
theme.widget_cpu                                = theme.confdir .. "/icons/cpu_dark.png"
-- theme.widget_weather                            = theme.confdir .. "/icons/dish.png"
-- theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_mem                                = theme.confdir .. "/icons/mem_dark.png"
theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_note                               = theme.confdir .. "/icons/note.png"
theme.widget_note_on                            = theme.confdir .. "/icons/note_on.png"
theme.widget_netdown                            = theme.confdir .. "/icons/net_down_dark.png"
theme.widget_netup                              = theme.confdir .. "/icons/net_up_dark.png"
theme.widget_mail                               = theme.confdir .. "/icons/mail.png"
theme.widget_batt                               = theme.confdir .. "/icons/bat_dark.png"
theme.widget_clock                              = theme.confdir .. "/icons/clock_dark.png"
theme.widget_vol                                = theme.confdir .. "/icons/spkr_dark.png"
theme.taglist_squares_sel                       = theme.confdir .. "/icons/square_a.png"
theme.taglist_squares_unsel                     = theme.confdir .. "/icons/square_b.png"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = true
theme.useless_gap                               = 0
theme.layout_tile                               = theme.confdir .. "/icons/tile.png"
theme.layout_tilegaps                           = theme.confdir .. "/icons/tilegaps.png"
theme.layout_tileleft                           = theme.confdir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.confdir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.confdir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.confdir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.confdir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.confdir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.confdir .. "/icons/dwindle.png"
theme.layout_max                                = theme.confdir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.confdir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.confdir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.confdir .. "/icons/floating.png"
theme.titlebar_close_button_normal              = theme.confdir .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.confdir .. "/icons/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.confdir .. "/icons/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.confdir .. "/icons/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.confdir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.confdir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.confdir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.confdir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.confdir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.confdir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.confdir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.confdir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.confdir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.confdir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.confdir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.confdir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.confdir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.confdir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.confdir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.confdir .. "/icons/titlebar/maximized_focus_active.png"

theme.bg0 = "#333333"
theme.bg1 = "#16936E"
theme.bg2 = "#2B74A1"
theme.bg3 = "#BA4E04"
theme.bg4 = "#F28519"
theme.bg5 = "#C6EEF3"
theme.bg6 = "#05639C"
theme.bg7 = "#CC8C8D"
theme.bg8 = "#875265"
theme.bg9 = "#DED113"

local markup = lain.util.markup

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local mytextclock = wibox.widget.textclock(markup.fontcolor(theme.font, "#333333", "#778baf", "%A %d %B ") .. markup.fontcolor(theme.font, "#333333", "#778baf", " %H:%M "))

local clockwidget = wibox.container.background(wibox.container.margin(wibox.widget {clockicon, mytextclock, layout = wibox.layout.align.horizontal }, 1, 1), "#778baf")
-- mytextclock.font = theme.font

-- Calendar
-- theme.cal = lain.widget.calendar({
--     attach_to = { mytextclock },
--     notification_preset = {
--         font = "xos4 Terminus 10",
--         fg   = theme.fg_normal,
--         bg   = theme.bg_normal
--     }
-- })

-- Weather
-- local weathericon = wibox.widget.imagebox(theme.widget_weather)
-- theme.weather = lain.widget.weather({
--     city_id = 2643743, -- placeholder (London)
--     notification_preset = { font = "xos4 Terminus 10", fg = theme.fg_normal },
--     weather_na_markup = markup.fontfg(theme.font, "#eca4c4", "N/A "),
--     settings = function()
--         descr = weather_now["weather"][1]["description"]:lower()
--         units = math.floor(weather_now["main"]["temp"])
--         widget:set_markup(markup.fontfg(theme.font, "#eca4c4", descr .. " @ " .. units .. "°C "))
--     end
-- })

-- / fs
-- local fsicon = wibox.widget.imagebox(theme.widget_fs)
-- theme.fs = lain.widget.fs({
--     options = "--exclude-type=tmpfs",
--     notification_preset = { font = "xos4 Terminus 10", fg = theme.fg_normal },
--     settings  = function()
--         widget:set_markup(markup.fontfg(theme.font, "#80d9d8", fs_now.used .. "% "))
--     end
-- })

-- /home fs
-- fsicon = wibox.widget.imagebox(beautiful.disk)
-- fsbar = wibox.widget {
--     forced_height    = 1,
--     forced_width     = 59,
--     color            = beautiful.fg_normal,
--     background_color = beautiful.bg_normal,
--     margins          = 1,
--     paddings         = 1,
--     ticks            = true,
--     ticks_size       = 6,
--     widget           = wibox.widget.progressbar,
-- }
-- fshome = lain.widgets.fs({
--     partition = "/home",
--     options = "--exclude-type=tmpfs",
--     settings  = function()
--         if fs_now.used < 90 then
--             fsbar:set_color(beautiful.fg_normal)
--         else
--             fsbar:set_color("#EB8F8F")
--         end
--         fsbar:set_value(fs_now.used / 100)
--     end
-- })
-- fsbg = wibox.container.background(fsbar, "#474747", shape.rectangle)
-- fswidget = wibox.container.margin(fsbg, 2, 7, 4, 4)

--[[ Mail IMAP check
-- commented because it needs to be set before use
local mailicon = wibox.widget.imagebox()
local mail = lain.widget.imap({
    timeout  = 180,
    server   = "server",
    mail     = "mail",
    password = "keyring get mail",
    settings = function()
        if mailcount > 0 then
            mailicon:set_image(theme.widget_mail)
            widget:set_markup(markup.fontfg(theme.font, "#cccccc", mailcount .. " "))
        else
            widget:set_text("")
            --mailicon:set_image() -- not working in 4.0
            mailicon._private.image = nil
            mailicon:emit_signal("widget::redraw_needed")
            mailicon:emit_signal("widget::layout_changed")
        end
    end
})
--]]

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#333333", cpu_now.usage .. "% "))
    end
})
local cpuwidget = wibox.container.background(wibox.container.margin(wibox.widget {cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, 1, 1), "#d42f69")

-- Coretemp
local tempicon = wibox.widget.imagebox(theme.widget_temp)
local temp = lain.widget.temp({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#333333", coretemp_now .. "°C "))
    end
})
local coretempwidget = wibox.container.background(wibox.container.margin(wibox.widget {tempicon, temp.widget, layout = wibox.layout.align.horizontal }, 1, 1), "#f1af5f")

-- Battery
local baticon = wibox.widget.imagebox(theme.widget_batt)
local bat = lain.widget.bat({
    settings = function()
        local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc

        if bat_now.ac_status == 1 then
            perc = perc .. " plug"
        end
        
        widget:set_markup(markup.fontfg(theme.font, "#333333", perc .. " "))
    end
})
local batterywidget = wibox.container.background(wibox.container.margin(wibox.widget { baticon, bat.widget, layout = wibox.layout.align.horizontal }, 1, 1), "#aaaaaa")

-- ALSA volume
local volicon = wibox.widget.imagebox(theme.widget_vol)
theme.volume = lain.widget.alsa({
    settings = function()
        if volume_now.status == "off" then
            volume_now.level = volume_now.level .. "M"
        end
        
        widget:set_markup(markup.fontfg(theme.font, "#333333", volume_now.level .. "% "))
    end
})
local volumewidget = wibox.container.background(wibox.container.margin(wibox.widget { volicon, theme.volume.widget, layout = wibox.layout.align.horizontal }, 1, 1), "#778baf")

-- Net
local netdownicon = wibox.widget.imagebox(theme.widget_netdown)
local netdowninfo = wibox.widget.textbox()
local netupicon = wibox.widget.imagebox(theme.widget_netup)
local netupinfo = lain.widget.net({
    settings = function()
        -- if iface ~= "network off" and
        --    string.match(theme.weather.widget.text, "N/A")
        -- then
            --     theme.weather.update()
            -- end
            
            widget:set_markup(markup.fontfg(theme.font, "#333333", net_now.sent .. " "))
            netdowninfo:set_markup(markup.fontfg(theme.font, "#333333", net_now.received .. " "))
        end
    })
local netupwidget = wibox.container.background(wibox.container.margin(wibox.widget { netupicon, netupinfo.widget, layout = wibox.layout.align.horizontal }, 1, 1), "#ce5666")
local netdownwidget = wibox.container.background(wibox.container.margin(wibox.widget { netdownicon, netdowninfo, layout = wibox.layout.align.horizontal }, 1, 1), "#87af5f")
    
-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local memory = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#333333", mem_now.used .. "M "))
    end
})
local memorywidget = wibox.container.background(wibox.container.margin(wibox.widget { memicon, memory.widget, layout = wibox.layout.align.horizontal }, 1, 1), "#e0da37")

function theme.at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake({ app = awful.util.terminal })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = 20, bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --s.mylayoutbox,
            s.mytaglist,
            s.mypromptbox,
        },
        --s.mytasklist, -- Middle widget
        nil,
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            wibox.widget.systray(),
            spotify_widget,
            --mailicon,
            --mail.widget,
            netdownwidget,
            netupwidget,
            volumewidget,
            memorywidget,
            cpuwidget,
            -- fsicon,
            -- theme.fs.widget,
            -- weathericon,
            -- theme.weather.widget,
            coretempwidget,
            batterywidget,
            clockwidget,
        },
    }

    -- Create the bottom wibox
    s.mybottomwibox = awful.wibar({ position = "bottom", screen = s, border_width = 0, height = 20, bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the bottom wibox
    s.mybottomwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            s.mylayoutbox,
        },
    }
end

return theme
