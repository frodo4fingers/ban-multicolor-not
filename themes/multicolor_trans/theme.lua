
--[[
                                     
     Multicolor Awesome WM theme 2.0
     github.com/copycat-killer       
                                     
--]]

local beautiful     = require("beautiful")
local gears = require("gears")
local lain  = require("lain")
local awful = require("awful")
local wibox = require("wibox")
local os    = { getenv = os.getenv, setlocale = os.setlocale }
local spotify_widget = require("awesome-wm-widgets.spotify-widget.spotify")
local vicious = require("vicious")

local theme                                     = {}
theme.confdir                                   = os.getenv("HOME") .. "/.config/awesome/themes/multicolor"
theme.wallpaper                                 = "/home/frodo/Bilder/Painting-City-Full-HD-Wallpapers.jpg"
-- theme.wallpaper                                 = "/home/frodo/Bilder/mount.png"
-- theme.wallpaper                                 = "/home/frodo/Bilder/tea.jpg"
-- theme.font                                      = "xos4 Terminus 8"
theme.font                                      = "Liberation Sans 10.5"
theme.trans                                     = "b4"
theme.menu_bg_normal                            = "#1B1C1C" .. theme.trans
theme.menu_bg_focus                             = "#1B1C1C" .. theme.trans
theme.bg_normal                                 = "#1B1C1C" .. theme.trans
theme.bg_focus                                  = "#ffffff" .. theme.trans -- "#0991FF"
theme.bg_urgent                                 = "#1B1C1C" .. theme.trans
theme.fg_normal                                 = "#ffffff" .. theme.trans
theme.fg_focus                                  = "#1B1C1C" .. theme.trans
theme.fg_urgent                                 = "#FF530D" .. theme.trans
theme.fg_minimize                               = "#656565" .. theme.trans
theme.border_width                              = 1
theme.border_normal                             = "#1c2022" .. theme.trans
theme.border_focus                              = "#dbdbdb" .. theme.trans -- "#0991FF"
theme.border_marked                             = "#656565" .. theme.trans -- "#0991FF"
theme.menu_border_width                         = 0
theme.menu_width                                = 160
theme.menu_submenu_icon                         = theme.confdir .. "/icons/submenu.png"
theme.menu_fg_normal                            = "#656565" .. theme.trans
theme.menu_fg_focus                             = "#656565" .. theme.trans -- "#0991FF"
theme.menu_bg_normal                            = "#1B1C1C" .. theme.trans
theme.menu_bg_focus                             = "#1B1C1C" .. theme.trans
theme.widget_temp                               = theme.confdir .. "/icons/temp.png"
theme.widget_uptime                             = theme.confdir .. "/icons/ac.png"
theme.widget_cpu                                = theme.confdir .. "/icons/cpu.png"
-- theme.widget_weather                            = theme.confdir .. "/icons/dish.png"
-- theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_mem                                = theme.confdir .. "/icons/mem.png"
theme.widget_fs                                 = theme.confdir .. "/icons/fs.png"
theme.widget_note                               = theme.confdir .. "/icons/note.png"
theme.widget_note_on                            = theme.confdir .. "/icons/note_on.png"
theme.widget_netdown                            = theme.confdir .. "/icons/net_down.png"
theme.widget_netup                              = theme.confdir .. "/icons/net_up.png"
theme.widget_mail                               = theme.confdir .. "/icons/mail.png"
theme.widget_batt                               = theme.confdir .. "/icons/bat.png"
theme.widget_clock                              = theme.confdir .. "/icons/clock.png"
theme.widget_vol                                = theme.confdir .. "/icons/spkr.png"
theme.taglist_squares_sel                       = theme.confdir .. "/icons/square_a.png"
theme.taglist_squares_unsel                     = theme.confdir .. "/icons/square_b.png"
theme.tasklist_plain_task_name                  = true
theme.tasklist_disable_icon                     = false
theme.useless_gap                               = 0
theme.layout_tile                               = theme.confdir .. "/icons/tile.png"
theme.layout_tilegaps                           = theme.confdir .. "/icons/tilegaps.png"
theme.layout_tileleft                           = theme.confdir .. "/icons/tileleft.png"
theme.layout_tilebottom                         = theme.confdir .. "/icons/tilebottom.png"
theme.layout_tiletop                            = theme.confdir .. "/icons/tiletop.png"
theme.layout_fairv                              = theme.confdir .. "/icons/fairv.png"
theme.layout_fairh                              = theme.confdir .. "/icons/fairh.png"
theme.layout_spiral                             = theme.confdir .. "/icons/spiral.png"
theme.layout_dwindle                            = theme.confdir .. "/icons/dwindle.png"
theme.layout_max                                = theme.confdir .. "/icons/max.png"
theme.layout_fullscreen                         = theme.confdir .. "/icons/fullscreen.png"
theme.layout_magnifier                          = theme.confdir .. "/icons/magnifier.png"
theme.layout_floating                           = theme.confdir .. "/icons/floating.png"
theme.titlebar_close_button_normal              = theme.confdir .. "/icons/titlebar/close_normal.png"
theme.titlebar_close_button_focus               = theme.confdir .. "/icons/titlebar/close_focus.png"
theme.titlebar_minimize_button_normal           = theme.confdir .. "/icons/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus            = theme.confdir .. "/icons/titlebar/minimize_focus.png"
theme.titlebar_ontop_button_normal_inactive     = theme.confdir .. "/icons/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive      = theme.confdir .. "/icons/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active       = theme.confdir .. "/icons/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active        = theme.confdir .. "/icons/titlebar/ontop_focus_active.png"
theme.titlebar_sticky_button_normal_inactive    = theme.confdir .. "/icons/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive     = theme.confdir .. "/icons/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active      = theme.confdir .. "/icons/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active       = theme.confdir .. "/icons/titlebar/sticky_focus_active.png"
theme.titlebar_floating_button_normal_inactive  = theme.confdir .. "/icons/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive   = theme.confdir .. "/icons/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active    = theme.confdir .. "/icons/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active     = theme.confdir .. "/icons/titlebar/floating_focus_active.png"
theme.titlebar_maximized_button_normal_inactive = theme.confdir .. "/icons/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.confdir .. "/icons/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active   = theme.confdir .. "/icons/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active    = theme.confdir .. "/icons/titlebar/maximized_focus_active.png"

theme.bg0 = "#1B1C1C" .. "00" -- "#1B1C1C" .. theme.trans
theme.bg1 = "#1B1C1C" .. "00" -- "#eef2f6" .. theme.trans -- "#ce5666"
theme.bg2 = "#1B1C1C" .. "00" -- "#ccd9e5" .. theme.trans -- "#87af5f"
theme.bg3 = "#1B1C1C" .. "00" -- "#abbfd4" .. theme.trans -- "#F28519"
theme.bg4 = "#1B1C1C" .. "00" -- "#89a6c2" .. theme.trans -- "#778baf"
theme.bg5 = "#1B1C1C" .. "00" -- "#678cb1" .. theme.trans -- "#e0da37"
theme.bg6 = "#1B1C1C" .. "00" -- "#4e7398" .. theme.trans -- "#d42f69"
theme.bg7 = "#1B1C1C" .. "00" -- "#3d5976" .. theme.trans
theme.bg8 = "#1B1C1C" .. "00" -- "#2b4054" .. theme.trans -- "#C6EEF3"
theme.bg9 = "#1B1C1C" .. "00" -- "#1a2633" .. theme.trans -- "#05639C"

local markup = lain.util.markup

-- Textclock
os.setlocale(os.getenv("LANG")) -- to localize the clock
local clockicon = wibox.widget.imagebox(theme.widget_clock)
local mytextclock = wibox.widget.textclock(markup.fontcolor(theme.font, "#fff", theme.bg9, "%A %d %B ") .. markup.fontcolor(theme.font, "#fff", theme.bg9, " %H:%M "))

local clockwidget = wibox.container.background(wibox.container.margin(wibox.widget {clockicon, mytextclock, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg9)

-- Battery
local baticon = wibox.widget.imagebox(theme.widget_batt)
local bat = lain.widget.bat({
    settings = function()
        local perc = bat_now.perc ~= "N/A" and bat_now.perc .. "%" or bat_now.perc

        if bat_now.ac_status == 1 then
            perc = perc .. " plug"
        end
        
        widget:set_markup(markup.fontfg(theme.font, "#fff", perc .. " "))
    end
})
local batterywidget = wibox.container.background(wibox.container.margin(wibox.widget { baticon, bat.widget, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg8)

-- Coretemp
local tempicon = wibox.widget.imagebox(theme.widget_temp)
local temp = lain.widget.temp({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#fff", coretemp_now .. "°C "))
    end
})
local coretempwidget = wibox.container.background(wibox.container.margin(wibox.widget {tempicon, temp.widget, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg7)

-- CPU
local cpuicon = wibox.widget.imagebox(theme.widget_cpu)
local cpu = lain.widget.cpu({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#fff", cpu_now.usage .. "% "))
    end
})
local cpuwidget = wibox.container.background(wibox.container.margin(wibox.widget {cpuicon, cpu.widget, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg6)

-- MEM
local memicon = wibox.widget.imagebox(theme.widget_mem)
local memory = lain.widget.mem({
    settings = function()
        widget:set_markup(markup.fontfg(theme.font, "#fff", mem_now.used .. "M "))
    end
})
local memorywidget = wibox.container.background(wibox.container.margin(wibox.widget { memicon, memory.widget, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg5)

-- ALSA volume
local volicon = wibox.widget.imagebox(theme.widget_vol)
theme.volume = lain.widget.alsa({
    settings = function()
        if volume_now.status == "off" then
            volume_now.level = volume_now.level .. "M"
        end
        
        widget:set_markup(markup.fontfg(theme.font, "#fff", volume_now.level .. "% "))
    end
})
local volumewidget = wibox.container.background(wibox.container.margin(wibox.widget { volicon, theme.volume.widget, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg4)

-- Net
local netdownicon = wibox.widget.imagebox(theme.widget_netdown)
local netdowninfo = wibox.widget.textbox()
local netupicon = wibox.widget.imagebox(theme.widget_netup)
local netupinfo = lain.widget.net({
    settings = function()
        -- if iface ~= "network off" and
        --    string.match(theme.weather.widget.text, "N/A")
        -- then
            --     theme.weather.update()
            -- end
            
            widget:set_markup(markup.fontfg(theme.font, "#fff", net_now.sent .. " "))
            netdowninfo:set_markup(markup.fontfg(theme.font, "#fff", net_now.received .. " "))
        end
    })
local netupwidget = wibox.container.background(wibox.container.margin(wibox.widget { netupicon, netupinfo.widget, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg3)
local netdownwidget = wibox.container.background(wibox.container.margin(wibox.widget { netdownicon, netdowninfo, layout = wibox.layout.align.horizontal }, 1, 1), theme.bg2)

-- spotify
local spotifywidget = wibox.container.background(wibox.container.margin(spotify_widget, 1, 1), theme.bg1)

-- systray
local systraywidget = wibox.widget.systray()
systraywidget.opacity = 1

-- local hddtempwidget = wibox.widget.textbox()
-- vicious.register(hddtempwidget, vicious.widgets.fs, "${/dev/nvme0n1p3} °C", 19)

function theme.at_screen_connect(s)
    -- Quake application
    s.quake = lain.util.quake({ app = awful.util.terminal })

    -- If wallpaper is a function, call it with the screen
    local wallpaper = theme.wallpaper
    if type(wallpaper) == "function" then
        wallpaper = wallpaper(s)
    end
    gears.wallpaper.maximized(wallpaper, s, true)

    -- Tags
    awful.tag(awful.util.tagnames, s, awful.layout.layouts)

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, awful.util.taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, awful.util.tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s, height = 20, bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            s.mytaglist,
            -- s.mypromptbox,
        },
        -- s.mytasklist, -- Middle widget
        nil,
        { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        systraywidget,
        spotifywidget,
        -- hddtempwidget,
        -- mailicon,
        -- mail.widget,
        netdownwidget,
        netupwidget,
        volumewidget,
        memorywidget,
        cpuwidget,
        -- fsicon,
        -- theme.fs.widget,
        -- weathericon,
        -- theme.weather.widget,
        coretempwidget,
        batterywidget,
        clockwidget,
        -- s.mylayoutbox,
        },
    }

    -- Create the bottom wibox
    s.mybottomwibox = awful.wibar({ position = "bottom", screen = s, border_width = 0, height = 20, bg = theme.bg_normal, fg = theme.fg_normal })

    -- Add widgets to the bottom wibox
    s.mybottomwibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            s.mylayoutbox,
        },
    }
end

return theme
